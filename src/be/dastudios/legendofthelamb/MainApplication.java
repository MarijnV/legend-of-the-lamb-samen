package be.dastudios.legendofthelamb;

import java.io.Serializable;
import java.util.Arrays;

/**
 * The main aplication runs a startupmenu, loads any locally saved maps and savegames, then runs the Main Menu,
 * The game has one standard map an automatic battle.
 * A player can create a party by naming them and chosing race and class,
 * afterwards he/she can travel the standard map with that party, automaticly battleing monsters along the way, either suriving or dying.
 * Verbeterpunten:
 * //TODO packaging van functionaliteiten en subpackaging vb: units in de unit zelf, battle aspecten en library
 * //TODO vaste data, zoals monsters en items de zogenaamde libraries, externe bestanden maken die dan met notepad kunnen gemaakt of geedit worden, vb: expansion pack, modding, not hard coded
 * //TODO extract read write GameFile in util class.
 * //TODO monster vs player, battle functionaliteit herzien, program to interface
 * //TODO add feature nieuwe map
 * //TODO add feature only see enviroment in travel mode LOOK
 * //TODO add feature rolling for random starting stats
 * //TODO add feature user is able to choose target manually
 * //TODO add feature skills, powers, magic
 * //TODO add feature events, wich present options, resulting in challenges and possible rewards
 * //TODO add feature option to rest to recover
 * //TODO add feature monster units travel while party is traveling
 * //TODO grant the units in party experience points afther resolving a battle
 * //TODO drop items from dead foes on a tile
 * //TODO add gold from defeated enemies in party inventory
 *
 */
public class MainApplication {
    public static void main(String[] args) {
        MainMenu.startupMenu();
        MainMenu.loadMapsAndSaveGames();
        MainMenu.mainMenu();
        System.out.println("Program is finished");
    }
}
