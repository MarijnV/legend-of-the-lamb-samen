package be.dastudios.legendofthelamb;

import be.dastudios.legendofthelamb.travel.ForestOfStreams;
import be.dastudios.legendofthelamb.travel.GameMap;
import be.dastudios.legendofthelamb.unit.Unit;
import be.dastudios.legendofthelamb.unit.UnitCreator;
import be.dastudios.legendofthelamb.util.KeyboardUtil;

import java.util.ArrayList;
import java.util.Collection;

public class MainMenu  {
    public static void featureNotAdded () {
        System.out.println("<< this feature is not added yet >>");
    }

    public static void startupMenu() {
        int length = 29;
        printLineOfSymbols("+", length);
        System.out.println("++ Legend of the Lamb - DA ++");
        printLineOfSymbols("+", length);
        System.out.println("--    DA Studios Belgium   --");
        printLineOfSymbols("-", length);
        System.out.println("--   By Gunther & Marijn   --");
        printLineOfSymbols("-", length);
    }

    public static void loadMapsAndSaveGames () {
        System.out.println("Loading maps and savegames");
        featureNotAdded();
        //TODO load maps and savegames method
        printLineOfSymbols("+", 12);
        System.out.println("++  DONE ++");
        printLineOfSymbols("+", 12);
    }

    public static void mainMenu() {

        while (true) {
            printLineOfSymbols("*", 40);
            displayMainMenuOptions();
            printLineOfSymbols("*", 40);
            switch (askForMainMenuChoice()) {
                case 0:
                    GameMap gameMap = new ForestOfStreams();
                    startNewGame(gameMap);
                    continue;
                case 1:
                    //TODO run loadGame()
                    GameMap gameMapRead = GameMap.readGameFromFile();
                    continueLoadedGame(gameMapRead);

                    continue;
                case 2:
                    //TODO run reset()
                    featureNotAdded();
                    continue;
                case 3:
                    displayControls();
                    continue;
                case 4: //TODO run SaveGame
                    featureNotAdded();
                    continue;
                case 5:
                    break;
            }
        break;
        }
    }

    private static void displayMainMenuOptions() {
        System.out.println("What do you want to do?");
        System.out.println("New      - Start a New Game");
        System.out.println("Load     - Load a Saved Game");
        System.out.println("Reset    - Delete some or all Saved Games");
        System.out.println("Controls - Game Controls");
        System.out.println("Settings - Game Settings");
        System.out.println("Quit     - Quit Game");
        System.out.println();
    }

    public static void displayControls() {
        System.out.println("Attention ! Don't use ' - ' symbol when inputting commands!");
        System.out.println("When traveling =>");
        System.out.println("Go -Direction- : go to direction");
        System.out.println("Direction : north, south, west, east");
        System.out.println("Save -saveName- : Saves the game under specified name");
        featureNotAdded();
        System.out.println("Quit : goes back to main menu without saving!");
        featureNotAdded();
    }

    private static void printLineOfSymbols(String symbol, int length) {
        for (int i = 0; i < length; i++) {
            System.out.print(symbol);
        }
        System.out.println();
    }

    public static ArrayList<String> getMainMenuActionWords () {
        ArrayList<String> mainMenuActionWords = new ArrayList<String>();
        mainMenuActionWords.add("NEW");
        mainMenuActionWords.add("LOAD");
        mainMenuActionWords.add("RESET");
        mainMenuActionWords.add("CONTROLS");
        mainMenuActionWords.add("SETTINGS");
        mainMenuActionWords.add("QUIT");
        return mainMenuActionWords;
    }

    public static int askForMainMenuChoice() {
        String temp = KeyboardUtil.askForActionWord(getMainMenuActionWords());
        return getMainMenuActionWords().indexOf(temp);
    }

    public static void startNewGame (GameMap gameMap) {
        System.out.println("You can only play in the -Forest Of Streams- map at the moment.");
        Collection<Unit> adventuringParty = new ArrayList<>();
        System.out.println("How many players do you want to play with?");
        int numberOfPlayers = Math.max( KeyboardUtil.askForNumber(), 1 );
        for (int i = 0 ; i < numberOfPlayers ; i++) {
            adventuringParty.add(UnitCreator.createPlayerCharacter());
        }
        gameMap.startJourney(adventuringParty);
    }

    public static void continueLoadedGame (GameMap gameMap) {
        gameMap.travel();
        System.out.println("You died");
    }

}
