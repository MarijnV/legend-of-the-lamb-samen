package be.dastudios.legendofthelamb.travel;

import java.io.Serializable;
import java.util.HashSet;

public class Coordinates implements Serializable {
    private int x;
    private int y;
    private static int mapSize;

    public Coordinates (int x, int y) {
        setX(x);
        setY(y);
        mapSize = ForestOfStreams.getMapSize();
        if (x < 0 || y < 0 || y > mapSize - 1 || x > mapSize - 1) {
            System.out.println("Warning : coordinates not valid");
        }

    }

    public static int getTileIndex(Coordinates coord) {
        return (coord.getY() * mapSize + coord.getX());
    }

    public static Coordinates getCoordinates(int index) {
        Coordinates coord = new Coordinates(0,0);
        coord.setX(index % mapSize);
        coord.setY((index / mapSize));
        return coord;
    }

    public static int shiftIndexNorth(int index) {
        int candidate = index - mapSize;
        if (candidate < 0) {
            System.out.println("You're hitting the north wall");
            return -1;
        }
        return candidate;
    }

    public static int shiftIndexSouth(int index) {
        int candidate = index + mapSize;
        if (candidate > mapSize * mapSize - 1) {
            System.out.println("You're hitting the south wall");
            return -1;
        }
        return candidate;
    }

    public static int shiftIndexWest(int index) {
        int candidate = index - 1;
        if (candidate % mapSize == mapSize - 1) {
            System.out.println("You're hitting the west wall");
            return -1;
        }
        return candidate;
    }

    public static int shiftIndexEast(int index) {
        int candidate = index + 1;
        if (candidate % mapSize == 0) {
            System.out.println("You're hitting the east wall");
            return -1;
        }
        return candidate;
    }

    public static HashSet<Direction> showAdjacentPathDirections(GameMap map, Coordinates coordinates) {
        HashSet<Direction> setToReturn = new HashSet<>();
        for (Direction dir : Direction.values()) {
            int indexMoved = moveToThe(dir, coordinates);
            if (indexMoved != -1) {
                Tile tileToCheck = map.getTile(indexMoved);
                if (tileToCheck instanceof WalkableTile) {
                    setToReturn.add(dir);
                }
            }
        }
        return setToReturn;
    }

    public static int moveToThe(Direction direction, Coordinates coordinates) {
        int toReturn = -1;

        int index = getTileIndex(coordinates);
        switch (direction) {
            case EAST:   toReturn = shiftIndexEast(index);                    break;
            case WEST:   toReturn = shiftIndexWest(index);                    break;
            case NORTH:   toReturn = shiftIndexNorth(index);                   break;
            case SOUTH:   toReturn = shiftIndexSouth(index);                   break;
        default     : toReturn = -1;                                           break;
        }
        return toReturn;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "["+getX()+"," + getY()+"]";
    }


}
