package be.dastudios.legendofthelamb.travel;

import java.io.Serializable;

public enum Direction implements Serializable {
    EAST ("east"),
    WEST ("west"),
    NORTH ("north"),
    SOUTH ("south"),
    SAVE ("save"),
    LOOK ("look");

    private String direction;

    Direction(String direction){
        setDirection(direction);
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
