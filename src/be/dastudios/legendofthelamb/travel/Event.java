package be.dastudios.legendofthelamb.travel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Event implements Serializable {
    private String decription;
    private List<Event> eventList;
    private static List<String> eventTypes = new ArrayList<>();

    public Event (String description) {
        setDecription(description);
    }

    public static void setEvenTypes () {
        eventTypes.add("Potion");
        eventTypes.add("Chest");
        eventTypes.add("Rubble");
        eventTypes.add("Storm");
    }

    public static List<Event> createMapEventList () {
        setEvenTypes();
        List<Event> mapEvents = new ArrayList<>();
        int size = (GameMap.MAP_SIZE * GameMap.MAP_SIZE);
        IntStream.range(0,size)
                 .forEach(t -> mapEvents.add(new Event(eventTypes.get(t % 4))));
        return mapEvents;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public void upDatePlayerAfterEvent() {
        // access player stats
    }
}
