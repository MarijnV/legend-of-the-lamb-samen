package be.dastudios.legendofthelamb.travel;

import be.dastudios.legendofthelamb.unit.MonsterLibrary;
import be.dastudios.legendofthelamb.unit.Unit;

import java.io.Serializable;
import java.util.*;

public class ForestOfStreams extends GameMap implements Serializable{
    private static final Coordinates startCoordinaten = new Coordinates(6,39);

    public ForestOfStreams() {
        super();
        createForestOfStreams();
        setCurrentPosition(startCoordinaten);
    }

    public void createForestOfStreams() {
        Event e1 = new Event("Potion");
        Event e2 = new Event("Potion");

        WalkableTile emptyPathTile = new WalkableTile("Path", null, null);

        lineOfRightGoingWalkables(new Coordinates(2,3),33, emptyPathTile); //A-3M-E
        lineOfDownGoingWalkables(new Coordinates(2,4),18, emptyPathTile);       //B
        lineOfRightGoingWalkables(new Coordinates(3,13),15, emptyPathTile);  //C-3M
        lineOfDownGoingWalkables(new Coordinates(18,4),11, emptyPathTile);      //D
        lineOfRightGoingWalkables(new Coordinates(19,14),8, emptyPathTile);     //E
        lineOfDownGoingWalkables(new Coordinates(34,4),28, emptyPathTile);   //F-2M
        lineOfRightGoingWalkables(new Coordinates(16,20),13, emptyPathTile);    //G
        lineOfRightGoingWalkables(new Coordinates(1,22),11, emptyPathTile);     //H
        lineOfDownGoingWalkables(new Coordinates(11,22),9, emptyPathTile);      //I
        lineOfDownGoingWalkables(new Coordinates(1,23),12, emptyPathTile);      //J
        lineOfDownGoingWalkables(new Coordinates(16,21),4, emptyPathTile);      //K
        lineOfRightGoingWalkables(new Coordinates(12,24),4, emptyPathTile);     //L
        lineOfRightGoingWalkables(new Coordinates(12,30),7, emptyPathTile);     //M
        lineOfDownGoingWalkables(new Coordinates(18,26),4, emptyPathTile);      //N
        lineOfRightGoingWalkables(new Coordinates(19,26),4, emptyPathTile);     //0
        lineOfDownGoingWalkables(new Coordinates(22,27),9, emptyPathTile);      //P
        lineOfDownGoingWalkables(new Coordinates(28,31),7, emptyPathTile);      //Q
        lineOfRightGoingWalkables(new Coordinates(29,31),5, emptyPathTile);     //R
        lineOfDownGoingWalkables(new Coordinates(32,32),5, emptyPathTile);      //S
        lineOfRightGoingWalkables(new Coordinates(2,34),7, emptyPathTile);      //T
        lineOfDownGoingWalkables(new Coordinates(8,35),3, emptyPathTile);       //U
        lineOfRightGoingWalkables(new Coordinates(6,37),2, emptyPathTile);      //V
        lineOfDownGoingWalkables(new Coordinates(6,38),2, emptyPathTile);       //W
        lineOfRightGoingWalkables(new Coordinates(17,35),5, emptyPathTile);     //1
        lineOfDownGoingWalkables(new Coordinates(17,36),2, emptyPathTile);      //2
        lineOfRightGoingWalkables(new Coordinates(18,37),11, emptyPathTile);    //3
        lineOfRightGoingWalkables(new Coordinates(30,36),2, emptyPathTile);     //4
        lineOfDownGoingWalkables(new Coordinates(30,37),2, emptyPathTile);      //5
        lineOfRightGoingWalkables(new Coordinates(31,38),8, emptyPathTile);     //6
        lineOfRightGoingWalkables(new Coordinates(33,33),6, emptyPathTile);     //7
        lineOfDownGoingWalkables(new Coordinates(38,34),4, emptyPathTile);      //8

        MonsterLibrary monsterLibrary = new MonsterLibrary();

        addEvent(new Coordinates(34,38), e1);
        addEventAndUnit(new Coordinates(36,38), e2, monsterLibrary.getNewMonster("Bugbear"));

        // FIRST BATTLE SCENE

        addUnit(new Coordinates(6,38),monsterLibrary.getNewMonster("goblin ranger"));

        //adding other monsters
        addUnitCollection(new Coordinates(8,37), monsterLibrary.getWolfPack() );
        addUnitCollection( new Coordinates(8,36),monsterLibrary.getGoblinRaidingParty() );
        addUnit( new Coordinates(5,34) , monsterLibrary.getNewMonster("troll") );
        addUnitCollection(new Coordinates(3,34), monsterLibrary.getGoblinRaidingParty() );
        Collection<Unit> omfg = new ArrayList<>();
        omfg.add(monsterLibrary.getNewMonster("troll"));
        omfg.add(monsterLibrary.getNewMonster("troll"));
        omfg.addAll(monsterLibrary.getGoblinRaidingParty());
        omfg.addAll( monsterLibrary.getWolfPack() );

        addUnitCollection( new Coordinates(1,33), omfg);

    }








}
