package be.dastudios.legendofthelamb.travel;

import be.dastudios.legendofthelamb.unit.Battle;
import be.dastudios.legendofthelamb.unit.Unit;
import be.dastudios.legendofthelamb.util.KeyboardUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.IntStream;

public class GameMap implements Serializable {
    protected static final int MAP_SIZE = 40;
    public List<Tile> tiles = new ArrayList<>();
    public Coordinates currentPosition;
    private int amplitude = 1;
    Tile emptyTile;
    Tile emptyWalkableTile;

    public GameMap() {
        emptyTile = new Tile("A forest (The Cure)");
        emptyWalkableTile = new Tile("A beautiful Path");
        IntStream.range(0,MAP_SIZE * MAP_SIZE )
                  //.forEach(t -> tiles.add(new Tile("A forest (The Cure)")));
                 .forEach(t -> tiles.add(emptyTile));
        currentPosition = (new Coordinates(6,39));
    }

    public void printMap () {
        for (int i = 0; i < MAP_SIZE * MAP_SIZE ; i++) {
            Tile tile = tiles.get(i);
            if (tile instanceof WalkableTile) {
                int unitNumber = ((WalkableTile) tile).getUnitsOnTile().size();
                int eventNumber   = ((WalkableTile) tile).getEventsInTile().size();
                boolean playerIsPresent = false;
                if (((WalkableTile) tile).getUnitsOnTile().size() > 0
                    && ((WalkableTile) tile).getUnitsOnTile().get(0).getName() != null) {
                    playerIsPresent = ((WalkableTile) tile).getUnitsOnTile().get(0).isPlayerControlled();
                    // .getName().equals("player");
                }
                if (unitNumber == 2) {
                    System.out.print("C  ");          // C ombat between 1 player and 1 monster
                }
                else if (unitNumber > 0 && eventNumber > 0) {
                    System.out.print("B  ");            // B oth monster and event
                } else if (unitNumber > 0 && playerIsPresent) {
                    System.out.print("P  ");           //  U nit only
                } else if (unitNumber > 0) {
                    System.out.print("U  ");
                } else if (eventNumber > 0 ){
                    System.out.print("E  ");           //  E vent only
                } else {
                    System.out.print("X  ");           //  X just an empty walkable tile
                }
            } else {
                System.out.print("-  ");
            }
            if (i % MAP_SIZE == MAP_SIZE - 1) {
                System.out.println();
            }
        }
        System.out.println();
        System.out.println("Legende | P only players  | U only monsters ");
        System.out.println("        | X walkable tile | - not walkable tile");
        System.out.println("        |E only events    |B both monsters and events ");
        System.out.println();
    }


    protected void lineOfRightGoingWalkables(Coordinates start, int number, Tile tile) {
        int x = start.getX();
        int y = start.getY();
        for (int i = 0; i < number; i++) {

            x = start.getX() + i;
            Coordinates tileToSet = new Coordinates(x,y);
            tiles.set(Coordinates.getTileIndex(tileToSet), new WalkableTile("Path", null, null));
            //tiles.set(Coordinates.getTileIndex(tileToSet), tile);
        }
    }

    protected void lineOfDownGoingWalkables(Coordinates start, int number, Tile  tile) {
        int x = start.getX();
        int y = start.getY();
        for (int i = 0; i < number; i++) {
            y = start.getY() + i;
            Coordinates tileToSet = new Coordinates(x,y);
            tiles.set(Coordinates.getTileIndex(tileToSet), new WalkableTile("Path", null, null));
        }
    }


    public Coordinates getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Coordinates currentPosition) {
        this.currentPosition = currentPosition;
    }

    public static int getMapSize() {
        return MAP_SIZE;
    }

    public Tile getTile(int index) {
        return tiles.get(index);
    }

    public void setTiles(List<Tile> tiles) {
        this.tiles = tiles;
    }

    /**
     * the first time a journey is started, the players get put on the starting tile
     * this last is set in the constructor of the SUBCLASS of GameMap
     * @param adventuringParty
     */
    public void startJourney (Collection<Unit> adventuringParty) {
       addUnitCollection(getCurrentPosition(), adventuringParty);
        travel();
    }

    /**
     * travel continues any journey from the
     * last known coordinates saved under currentPosition
     */
    public void travel() {
        while ( isAnyPlayerAliveOnTile( getCurrentPosition() ) ) {
            int newTileIndex = describeAndChoseDirection( getCurrentPosition() );
            //TODO split describing and asking for input,
            // this allows for more high cohesion methods,
            // and allows to parse the analyse string for other ACTION WORDS, such as SAVE or QUIT
            moveAllUnit(Coordinates.getTileIndex(getCurrentPosition()), newTileIndex );
            fightIfNecessary(newTileIndex);
            setCurrentPosition(Coordinates.getCoordinates(newTileIndex));
        }
        System.out.println("\nAll members of your adventuring party have retired early.");
    }

    public int describeAndChoseDirection(Coordinates coordinates) {
        HashSet<Direction> directions = Coordinates.showAdjacentPathDirections(this, coordinates);
        Direction playerChosenDirection;
        do {
            System.out.print("You are at " + coordinates + " You can go ");
            directions.forEach(t -> System.out.print(" " + t.getDirection() + " "));
            System.out.println();
            playerChosenDirection = KeyboardUtil.askForDirection(); //TODO in feite kan askForDirection een HashSet Aanvaarden en onmiddelijk een legale richting bevatten
            if (playerChosenDirection.equals(Direction.SAVE)) {
                GameMap.writeGameTofile(this);
            }
            if (playerChosenDirection.equals(Direction.LOOK)) {
                printMap();
            }
        } while (!directions.contains(playerChosenDirection));
        return Coordinates.moveToThe(playerChosenDirection, coordinates);
    }

    public void moveFirstUnit(int oldTileIndex, int newTileIndex) {
        Unit thisPlayer =  ((WalkableTile) tiles.get(oldTileIndex)).getUnitsOnTile().get(0); //
        ((WalkableTile) tiles.get(oldTileIndex)).getUnitsOnTile().remove(0);
        ((WalkableTile) tiles.get(newTileIndex)).getUnitsOnTile().add(0, thisPlayer);
    }

    public void moveAllUnit (int oldTileIndex, int newTileIndex) {
        ( (WalkableTile) tiles.get(newTileIndex) ).getUnitsOnTile().addAll( ((WalkableTile) tiles.get(oldTileIndex)).getUnitsOnTile() );
        ((WalkableTile) tiles.get(oldTileIndex)).getUnitsOnTile().clear(); // this method clears all Units of the old tile
    }

    //TODO downcast to arrayList might not be nessesary if Battle Accepts a Collection
    public void fightIfNecessary(int tileIndex) {
        Battle battle = new Battle( (ArrayList<Unit>) ( (WalkableTile) tiles.get(tileIndex) ).getUnitsOnTile());
        if ( battle.isAnyNonPlayerControlledUnitAlive() ) {
            ((WalkableTile) tiles.get(tileIndex)).setUnitsOnTile(battle.fight());
        }
    }

@Deprecated
    public void movePlayer(GameMap map, Coordinates position) {
//        HashSet<Direction> directions = new HashSet<>();
        HashSet<Direction> directions = Coordinates.showAdjacentPathDirections(map, position);
        int oldTileIndex = Coordinates.getTileIndex(position);

        System.out.print("    --  You can go ");
        directions.forEach(t -> System.out.print(" " + t.getDirection() + " "));
        System.out.println();
//        int newTileIndex = -1;

        Direction playerChosenDirection;
        do {
            playerChosenDirection = KeyboardUtil.askForDirection();
        } while (!directions.contains(playerChosenDirection));
        int newTileIndex = Coordinates.moveToThe(playerChosenDirection, position);

        Unit thisPlayer =  ((WalkableTile) tiles.get(oldTileIndex)).getUnitsOnTile().get(0); //
        ((WalkableTile) tiles.get(oldTileIndex)).getUnitsOnTile().remove(0);
        ((WalkableTile) tiles.get(newTileIndex)).getUnitsOnTile().add(0, thisPlayer);

        Battle battle = new Battle((ArrayList<Unit>) ( (WalkableTile) tiles.get(newTileIndex) ).getUnitsOnTile());
        if ( battle.isAnyNonPlayerControlledUnitAlive() ) {
            ((WalkableTile) tiles.get(newTileIndex)).setUnitsOnTile(battle.fight());
        }

    }


    //TODO this method only allows for one monster to ever be added to a coordinate.
    protected void addUnit(Coordinates coordinates, Unit monster) {
        WalkableTile tileToAdd = new WalkableTile("Path", null, null);
        tileToAdd.getUnitsOnTile().add(monster);
        tiles.set(Coordinates.getTileIndex(coordinates),tileToAdd);
    }

    protected void addUnitCollection(Coordinates coordinates, Collection<Unit> units) {
        WalkableTile tileToAdd = new WalkableTile("Path", null, null);
        tileToAdd.getUnitsOnTile().addAll(units);
        tiles.set(Coordinates.getTileIndex(coordinates),tileToAdd);
    }

    protected void addEvent(Coordinates coordinates, Event event) {
        WalkableTile tileToAdd = new WalkableTile("Path", null, null);
        tileToAdd.getEventsInTile().add(event);
        tiles.set(Coordinates.getTileIndex(coordinates),tileToAdd);
    }

    protected void addEventAndUnit(Coordinates coordinates, Event event, Unit unit) {
        WalkableTile tileToAdd = new WalkableTile("Path", null, null);
        tileToAdd.getEventsInTile().add(event);
        tileToAdd.getUnitsOnTile().add(unit);
        tiles.set(Coordinates.getTileIndex(coordinates), tileToAdd);
    }

    public boolean isAnyPlayerAliveOnTile (Coordinates coordinates) {
        WalkableTile givenTile = (WalkableTile) tiles.get( Coordinates.getTileIndex( coordinates ) );
        Battle combatMedic = new Battle ( (ArrayList<Unit> ) givenTile.getUnitsOnTile() );
        return combatMedic.isAnyPlayerControlledUnitAlive();
    }

    public static void writeGameTofile(GameMap map) {
        try (
                FileOutputStream gamewriter = new FileOutputStream("GameFile.txt");
                ObjectOutputStream out = new ObjectOutputStream(gamewriter)) {
            out.writeObject(map);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static GameMap readGameFromFile() {
        try (FileInputStream gamereader = new FileInputStream("GameFile.txt");
             ObjectInputStream in = new ObjectInputStream(gamereader)) {
             GameMap forestOfStreamsRead = (GameMap) in.readObject();
             return forestOfStreamsRead;
        } catch (IOException | ClassNotFoundException ioe) {
             ioe.printStackTrace();
        }
        return null;
    }

}
