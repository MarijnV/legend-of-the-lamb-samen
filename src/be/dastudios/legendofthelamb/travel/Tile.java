package be.dastudios.legendofthelamb.travel;

import java.io.Serializable;
import java.util.List;

public class Tile implements Serializable {
    private String landscape = "A dark forest";

    public Tile ( String landscape) {
        setLandscape(landscape);
    }

    public String getLandscape() {
        return landscape;
    }

    public void setLandscape(String landscape) {
        this.landscape = landscape;
    }


}
