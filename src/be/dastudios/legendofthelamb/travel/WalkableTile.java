package be.dastudios.legendofthelamb.travel;

import be.dastudios.legendofthelamb.unit.Unit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WalkableTile extends Tile implements Serializable {
    private List<Unit> unitsOnTile = new ArrayList<>();
    private List<Event> eventsForTile = new ArrayList<>();

    public WalkableTile (String landscape, List unitsOnTile, List eventsInTile) {
        super(landscape);
    }



    public List<Unit> getUnitsOnTile() {
        return unitsOnTile;
    }

    public void setUnitsOnTile(List<Unit> unitsOnTile) {
        this.unitsOnTile = unitsOnTile;
    }

    public List<Event> getEventsInTile() {
        return eventsForTile;
    }

    public void setEventsInTile(List<Event> eventsInTile) {
        this.eventsForTile = eventsInTile;
    }

    public void addUnitOntoTile(Unit unit) {
        unitsOnTile.add(unit);
    }

    public void addEventForTile(Event event) {
        eventsForTile.add(event);
    }

    @Override
    public String toString() {
        return "WalkableTile{" +
                "unitsOnTile=" + unitsOnTile +
                ", eventsForTile=" + eventsForTile +
                '}';
    }
}
