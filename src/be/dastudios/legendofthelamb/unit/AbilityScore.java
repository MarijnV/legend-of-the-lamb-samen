package be.dastudios.legendofthelamb.unit;

import java.io.Serializable;

/**
 * This enum contains the 6 possible AbilityScores of a Unit AKA Attribute Scores
 */
public enum AbilityScore implements Serializable {
    STR ("Strength"),
    DEX ("Dexterity"),
    CON ("Constitution"),
    WIS ("Wisdom"),
    INT ("Intelligence"),
    CHA ("Charisma");

    String name;

    AbilityScore(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
