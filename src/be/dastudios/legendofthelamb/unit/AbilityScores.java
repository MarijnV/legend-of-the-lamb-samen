package be.dastudios.legendofthelamb.unit;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * This class contains a Map of all Ability Scores and their coresponding (Integer) int value
 */
public class AbilityScores implements Serializable {
    Map<AbilityScore, Integer> abilityScoresMap;

    /**
     * The empty constructor of AbilityScores fills all AbilityScores with ZERO
     */
    public AbilityScores() {
        this(0,0,0,0,0,0);
    }

    /**
     * this constructor creates an AbilityScoreMap with the given number value for all abilityScores
     * @param number
     */
    public AbilityScores (int number) {
        this (number, number, number, number, number, number);
    }

    /**
     * This constructor puts all given int values in the ASMap corresponding with their respective AbilityScore Enum
     * @param strength
     * @param dexterity
     * @param constitution
     * @param intelligence
     * @param wisdom
     * @param charisma
     */

    public AbilityScores(int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma) {
        setAbilityScoresMap(new HashMap<>());
        setAbilityScore(AbilityScore.STR, strength);
        setAbilityScore(AbilityScore.DEX, dexterity);
        setAbilityScore(AbilityScore.CON, constitution);
        setAbilityScore(AbilityScore.INT, intelligence);
        setAbilityScore(AbilityScore.WIS, wisdom);
        setAbilityScore(AbilityScore.CHA, charisma);
    }

    public Map<AbilityScore, Integer> getAbilityScoresMap() {
        return this.abilityScoresMap;
    }

    public void setAbilityScoresMap(Map<AbilityScore, Integer> abilityScoresMap) {
        this.abilityScoresMap = abilityScoresMap;
    }

    public int getAbilityScore(AbilityScore abilityScore) {
        return getAbilityScoresMap().get(abilityScore);
    }

    /**
     * setting an abilityscore based on the enum abilityscore and a given value
     * this method cappes at 20
     * @param abilityScore
     * @param value
     */
    public void setAbilityScore(AbilityScore abilityScore, int value) {
        getAbilityScoresMap().put(abilityScore, Math.min(value, 20));
    }

    //MEHTODEN


    private int abilityScoreToModifier(int abilityScoreValue) {
        return (abilityScoreValue - 10) / 2;
    }

    /**
     * This method gives a Modifier value corresponding to a given Ability Score
     * @param abilityScore
     * @return -5 ~ +5
     */
    public int getModifier(AbilityScore abilityScore) {
        return abilityScoreToModifier( getAbilityScore(abilityScore) );
    }

    /**
     * this method adds a given the AbilityScore values of a given AbilityScores
     * to the current AbilityScore values
     * @param bonuses
     */
    public void add(AbilityScores bonuses) {
        for (AbilityScore as : getAbilityScoresMap().keySet()) {
            setAbilityScore(as, getAbilityScore(as) + bonuses.getAbilityScore(as));
        }
    }

    @Override
    public String toString() {
        return "AbilityScores{" +
                "abilityScoresMap=" + abilityScoresMap +
                '}';
    }
}
