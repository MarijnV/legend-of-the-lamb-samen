package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.unit.AbilityScore;
import be.dastudios.legendofthelamb.unit.Item;

/**
 * This class extends Item class
 * As such it has a name
 * It adds a base armor score, which helps to determine Armor Class of the equipper
 * It adds an AbilityScore as base for Armor Class calculation of the equipper
 */
public class Armor extends Item {
    private AbilityScore baseAbilityScore;
    private int armorBase;
    public Armor(String name, AbilityScore baseAbilityScore, int armorBase) {
        super(name);
        setBaseAbilityScore(baseAbilityScore);
        setArmorBase(armorBase);
    }

    public AbilityScore getBaseAbilityScore() {
        return baseAbilityScore;
    }

    public void setBaseAbilityScore(AbilityScore baseAbilityScore) {
        this.baseAbilityScore = baseAbilityScore;
    }

    public int getArmorBase() {
        return armorBase;
    }

    public void setArmorBase(int armorBase) {
        this.armorBase = armorBase;
    }
}
