package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.unit.ItemLibrary;
import be.dastudios.legendofthelamb.unit.Unit;
import be.dastudios.legendofthelamb.unit.Weapon;
import be.dastudios.legendofthelamb.util.Roll;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class allows a group of units to battle
 */

public class Battle implements Serializable {
    private ArrayList<Unit> localUnitList;
    private int roundCounter;

    public Battle () {
    }

//TODO can Battle work with a Collection of Units instead
    /**
     * A new battle object needs a ArrayList of units
     * @param localUnitList
     */
    public Battle (ArrayList<Unit> localUnitList) {
        setRoundCounter(0);
        setLocalUnitList(localUnitList);
    }

    /**
     * returns all Units present in current combat
     * @return
     */
    public ArrayList<Unit> getLocalUnitList() {
        return localUnitList;
    }

    public void setLocalUnitList(ArrayList<Unit> localUnitList) {
        this.localUnitList = localUnitList;
    }

    public int getRoundCounter() {
        return roundCounter;
    }

    public void setRoundCounter(int roundCounter) {
        this.roundCounter = roundCounter;
    }

    public void incrementRoundCounter() {
        roundCounter++;
    }

    /**
     * this method lets all Units roll4initiative
     * and take turns in order, attacking using their best weapon against an enemy creature.
     * all dead units are removed afterwards
     * @return
     */
    public ArrayList<Unit> fight () {
        rollInitiative();
        printInitiativeOrder();
        while ( isAnyPlayerControlledUnitAlive() && isAnyNonPlayerControlledUnitAlive() ) {
            incrementRoundCounter();
            System.out.println(this);
            getLocalUnitList().stream()
                    .filter(Unit::isAlive)
                    .filter(this::isEnemyAlive)
                    .forEach( unit -> basicWeaponAttack(unit, getTarget(unit) ) );
        }
        getLocalUnitList().removeIf( unit -> !unit.isAlive() );
        printEndReport();
        return getLocalUnitList();
    }

    /**
     * Checks if an enemy of a given unit is alive
     * @param unit
     * @return
     */
    public boolean isEnemyAlive (Unit unit) {
        return unit.isPlayerControlled() ? isAnyNonPlayerControlledUnitAlive() : isAnyPlayerControlledUnitAlive();
    }

    /**
     * this method returns any unit in the current battle who is alive and an enemy
     * @param activeUnit
     * @return
     */
    public Unit getTarget (Unit activeUnit) {
        return getLocalUnitList().stream()
                .filter( unit -> ( unit.isPlayerControlled() != activeUnit.isPlayerControlled() ) && unit.isAlive())
                .findAny().get();
    }

//TODO try to let a player pick a target and monsters just find first enemy
    public void takeTurn(Unit activeUnit) {

        if (activeUnit.isPlayerControlled()) {
            System.out.println("were letting u pick an power now");
//            attack();
            System.out.println("you could now pick and extra action");
        } else {
            System.out.println("the monster uses its first power");
//            attack();
        }
    }

    /**
     * this method lets the unit use his best weapon to attack another unit
     * @param activeUnit
     * @param target
     */
    public void basicWeaponAttack(Unit activeUnit, Unit target) {
        Weapon bestWeapon = getBestWeapon(activeUnit);
        int difficulty = target.getArmorClass();
        System.out.printf("%s tries to attack %s (AC= %d) with his %s\n", activeUnit.getName(), target.getName(), target.getArmorClass(), bestWeapon.getItemName().toLowerCase());
        if ( activeUnit.testSucces( bestWeapon.getBaseAbilityScore(), difficulty )) {  //&& activeUnit.isAlive()
            target.takesDamage(Roll.dX( bestWeapon.getDamageDiceSize() ) + activeUnit.getModifier( bestWeapon.getBaseAbilityScore() ) );
        }
    }

    /**
     * this method returns the weapon with the highest damageDice in a unit's inventory
     * this method is broken, if there is no weapon present in unit's inventory, it returns a dagger
     * always presuming a unit has a dagger
     * @param unit
     * @return
     */
    public Weapon getBestWeapon(Unit unit) {
        if (unit.getInventory().getItemList().stream().anyMatch(item -> item instanceof Weapon)) {
            int bestDamageDiceSize = unit.getInventory().getItemList().stream().filter(item -> item instanceof Weapon).mapToInt(item -> ((Weapon) item).getDamageDiceSize()).max().getAsInt();
            return (Weapon) unit.getInventory().getItemList().stream().filter(item -> item instanceof Weapon).filter(item -> ( (Weapon) item ).getDamageDiceSize() == bestDamageDiceSize ).findFirst().get();
        } else {
            return ( (Weapon) new ItemLibrary().getItem("Dagger") );
        }
    }

    /**
     * Returns true if at least one PlayerControlled unit is alive.
     * @return
     */
    public boolean isAnyPlayerControlledUnitAlive() {
        return getLocalUnitList().stream().filter( unit -> unit.isAlive() && unit.isPlayerControlled() ).count() > 0;
    }

    /**
     * Returns True if at least one NonPlayerControlled unit is alive.
     * @return
     */
    public boolean isAnyNonPlayerControlledUnitAlive() {
        return getLocalUnitList().stream().filter( unit -> unit.isAlive() && !unit.isPlayerControlled() ).count() > 0;
    }

    /**
     * this method makes all localUnits roll for initiative
     * and sorts them from highest to lowest
     */
    public void rollInitiative () {
        System.out.println("*******************************");
        System.out.println("Everybody roll for initiative !");
        getLocalUnitList().forEach(Unit::roll4Initiative);
        getLocalUnitList().sort( (u1,u2) -> u2.getCurrentInitiative() - u1.getCurrentInitiative() );
    }

    public void printInitiativeOrder () {
        System.out.print("Units according to initiative:");
        getLocalUnitList().forEach(u -> System.out.printf("\n%d - %s Initiative= %d", getLocalUnitList().indexOf(u) + 1 , u.getName(), u.getCurrentInitiative()));
        System.out.println();
    }

    public void printEndReport () {
        System.out.println("**************************");
        System.out.println("**      SURVIVORS       **");
        System.out.println("**************************");
        getLocalUnitList().forEach(u -> System.out.println(u.getName() + " " + u.getCombatStatsString()));
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder();
        temp.append("\n--== Round ==-- ").append( getRoundCounter() );
        getLocalUnitList().forEach(unit -> temp.append("\n").append(unit.getName()).append(" = ").append( unit.getCombatStatsString() ));
        temp.append("\n--== infoend ==--");
        return temp.toString();
    }
}
