package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.unit.Item;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class contains an ArrayList of items
 * and a maximum of 500 gold
 */
public class Inventory implements Serializable {
    private ArrayList<Item> itemList;
    private int goldAmount;
    private final int MAX_GOLD_AMOUNT = 500;

    public Inventory() {
        setItemList( new ArrayList<Item>() );
    }

    public ArrayList<Item> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<Item> itemList) {
        this.itemList = itemList;
    }

    public int getGoldAmount() {
        return goldAmount;
    }

    private void setGoldAmount(int goldAmount) {
        this.goldAmount = goldAmount;
    }

    public void addGold (int amount) {
        setGoldAmount( Math.min( getGoldAmount() + amount , MAX_GOLD_AMOUNT ) );
    }

    public void addItem (Item item) {
        getItemList().add(item);
    }

    public void removeItem (Item item) {
        getItemList().remove(item);
    }

    public String getItemListString () {
        StringBuilder temp = new StringBuilder();
        getItemList().forEach( item -> temp.append(item.toString()).append("\n") );
        return temp.toString();
    }

    @Override
    public String toString() {
        return "Inventory - " + getGoldAmount() + " gold pieces" +
                " itemList\n" + getItemListString();
    }
}
