package be.dastudios.legendofthelamb.unit;

import java.io.Serializable;

public class Item implements Serializable {
    private String itemName;
//TODO heeft nog geen description
    public Item () {
    }

    public Item (String itemName) {
       setItemName(itemName);
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public String toString() {
        return getItemName();
    }
}
