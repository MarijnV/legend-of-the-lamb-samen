package be.dastudios.legendofthelamb.unit;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Construction of an object of this class, creates a hashMap of Items, with their name as key
 * Which can be called apon by corresponding methods
 */
public class ItemLibrary implements Serializable {
    private HashMap<String, Item> itemLibrary = new HashMap<>();

    public ItemLibrary() {
        //TODO verliezen deze items functionaliteit als ze als Item aan de lijst worden toegevoegd
        Armor leatherArmor = new Armor("LEATHER ARMOR", AbilityScore.DEX, 6);
        add(leatherArmor);
        Armor chainMail = new Armor("CHAINMAIL", AbilityScore.STR, 8);
        add( chainMail );
        Armor clothes = new Armor("CLOTHES", AbilityScore.DEX, 4);
        add(clothes);
        Weapon sword = new Weapon("SWORD", AbilityScore.STR, 10);
        add( sword );
        Weapon bow = new Weapon("BOW", AbilityScore.DEX, 12);
        add( bow );
        //TODO dagger does not yet use highest of STR or DEX
        Weapon dagger = new Weapon("DAGGER", AbilityScore.STR, 4);
        add( dagger );
        Weapon holySymbol = new Weapon("HOLY SYMBOL", AbilityScore.WIS, 6);
        add( holySymbol );
        Weapon claws = new Weapon ("CLAWS", AbilityScore.STR, 6);
        add( claws );
    }

    public HashMap<String, Item> getItemLibrary() {
        return itemLibrary;
    }

    public void setItemLibrary(HashMap<String, Item> itemLibrary) {
        this.itemLibrary = itemLibrary;
    }

    public void add(Item item) {
        getItemLibrary().put(item.getItemName(), item);
    }

    /**
     * this method is invoked on a new ItemLibrary object
     * it returns the Item based on the stringname
     * @param itemname
     * @return
     */
    public Item getItem(String itemname) {
        return getItemLibrary().get(itemname.toUpperCase());
    }
}
