package be.dastudios.legendofthelamb.unit;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * deze klasse bevat geen functionaliteit, stelt het idee voor om monsters en spelers een lijst van 'moves', magie te geven
 * die kan gekozen kan worden tijdens combat
 */
public class ListOfPowers implements Serializable {
    private ArrayList<Power>[] listOfAbilities;

    public ListOfPowers() {

    }

    public ArrayList<Power>[] getListOfAbilities() {
        return listOfAbilities;
    }

    public void setListOfAbilities(ArrayList<Power>[] listOfAbilities) {
        this.listOfAbilities = listOfAbilities;
    }
}
