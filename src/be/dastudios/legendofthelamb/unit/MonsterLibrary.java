package be.dastudios.legendofthelamb.unit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * an object of the MonsterLibrary contains a HashMap of Monster Units
 * they can be called apon as new objects with the getNewMonster method, which accepts the monster name
 */
public class MonsterLibrary implements Serializable {
    private HashMap<String, Unit> monsterLibrary = new HashMap<>();

    public MonsterLibrary() {
        Unit goblinMinion = UnitCreator.createMonster("GOBLIN MINION", UnitRace.GOBLIN, UnitType.FIGHTER, 1, 1/8d);
        goblinMinion.addItem("Sword");
        goblinMinion.addItem("Leather armor");
        add(goblinMinion);
        Unit goblinFighter = UnitCreator.createMonster("GOBLIN FIGHTER", UnitRace.GOBLIN, UnitType.FIGHTER, 15, 1);
        goblinFighter.addItem("Sword");
        goblinFighter.addItem("Leather armor");
        add(goblinFighter);
        Unit goblinRanger = UnitCreator.createMonster("GOBLIN RANGER", UnitRace.GOBLIN, UnitType.RANGER, 12, 1);
        goblinRanger.addItem("Bow");
        goblinRanger.addItem("Leather armor");
        add(goblinRanger);
        Unit bugbear = UnitCreator.createMonster("BUGBEAR", UnitRace.BUGBEAR, UnitType.FIGHTER, 34, 3);
        bugbear.addItem("Sword");
        bugbear.addItem("Leather armor");
        add(bugbear);
        Unit wolf = UnitCreator.createMonster("WOLF", UnitRace.WOLF, UnitType.BEAST, 15, 1/2d);
        wolf.addItem("Chainmail");
        wolf.addItem("claws");
        add(wolf);
        Unit hobgoblin = UnitCreator.createMonster("HOBGOBLIN", UnitRace.HOBGOBLIN, UnitType.FIGHTER, 25, 2);
        hobgoblin.addItem("Sword");
        hobgoblin.addItem("Leather armor");
        add(hobgoblin);
        Unit troll = UnitCreator.createMonster("TROLL", UnitRace.TROLL, UnitType.FIGHTER, 50, 4);
        troll.addItem("Sword");
        troll.addItem("Chainmail");
        add(troll);
    }

    public HashMap<String, Unit> getMonsterLibrary() {
        return monsterLibrary;
    }

    public void setMonsterLibrary(HashMap<String, Unit> monsterLibrary) {
        this.monsterLibrary = monsterLibrary;
    }

    public void add(Unit unit) {
        getMonsterLibrary().put(unit.getName(), unit);
    }

    public Unit getNewMonster(String monsterName) {
        return new Unit ( getMonsterLibrary().get(monsterName.toUpperCase()) );
    }

    public Collection<Unit> getGoblinRaidingParty () {
        Collection<Unit> goblinRaidingParty = new ArrayList<>();
        goblinRaidingParty.add(getNewMonster("goblin minion"));
        goblinRaidingParty.add(getNewMonster("goblin minion"));
        goblinRaidingParty.add(getNewMonster("goblin minion"));
        goblinRaidingParty.add(getNewMonster("goblin fighter"));
        goblinRaidingParty.add(getNewMonster("goblin fighter"));
        goblinRaidingParty.add(getNewMonster("goblin ranger"));
        goblinRaidingParty.add(getNewMonster("goblin ranger"));
        return goblinRaidingParty;
    }

    public Collection<Unit> getWolfPack () {
        Collection<Unit> wolfpack = new ArrayList<>();
        wolfpack.add(getNewMonster("Wolf"));
        wolfpack.add(getNewMonster("Wolf"));
        wolfpack.add(getNewMonster("Wolf"));
        return wolfpack;
    }
}
