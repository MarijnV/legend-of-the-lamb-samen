package be.dastudios.legendofthelamb.unit;

import java.io.Serializable;

/**
 * A power represents a move or magic that interacts with units in combat
 * it has a name and a discription
 * and should have an effect/result/action (not implemented yet)
 * Level of availability allows the unit access to more powers apon reaching higher levels
 * either powers should be added to a unit apon leveling, or the possible powers should already be present in his/her list but enabled/disabled depending on level
 */
public class Power implements Serializable {
    private String name;
    private String description;
    private int levelOfAvailability;

    public Power(String name, String description) {
        setName(name);
        setDescription(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
