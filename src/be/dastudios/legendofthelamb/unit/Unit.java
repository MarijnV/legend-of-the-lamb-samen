package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.util.Roll;

import java.io.Serializable;
import java.util.stream.Stream;

/**
 * a Unit object hold everything a unit might need, except interacting with other units.
 */
public class Unit implements Serializable {
    //VIRTUAL FIELDS ARE:
    //Armor Class
    //Speed

    //FIELDS
    private String name;
    private int currentHitPoints;
    private int maxHitPoints;
    private ListOfPowers listOfPowers;
    private Inventory inventory;
    private AbilityScores abilityScores;
    private UnitRace race;
    private UnitType type;
    private int experiencePoints;
    private double level;
    private int currentInitiative = 10;
    private boolean isPlayerControlled = false;

    //CONSTRUCTORS

    /**
     * the empty constructor makes a enemy wolf
     */
    public Unit() {
        this("Wolf", UnitRace.WOLF, UnitType.BEAST, false);
        this.addItem("claws");
        this.addItem("Chainmail");
    }

    /**
     * the standard constructor of Unit accepts a String, Race, Type, boolean
     * the String indicates the name of the Unit
     * the boolean is to indicate if the Unit is under PlayerControl
     * Unit's Ability Scores are based on the standard array, according to the priority of the Type
     * then bonuses from Type and Race are added
     * Finaly, the max HP is rolled based on the Type (current HP adjusted as such)
     * Powers are not yet implemented
     * Inventory starts out empty, and is added in UnitCreation class or MonsterLibrary
     * @param name
     * @param race
     * @param type
     * @param isPlayerControlled
     */
    public Unit(String name, UnitRace race, UnitType type, boolean isPlayerControlled) {
        isPlayerControlled(isPlayerControlled);
        setName(name);
        setType(type);
        setRace(race);

        setAbilityScores(new AbilityScores());//we moeten een lege meegeven, om dubbele verwijzingen te fixen
        getAbilityScores().add( type.getInitialAbilityScoreArray() );
        getAbilityScores().add( type.getAbilityScoreBonuses() );
        getAbilityScores().add( race.getAbilityScoreBonuses() );

        setMaxHitPoints( rollLifeDice() + rollLifeDice() + rollLifeDice()  + 10);
        setCurrentHitPoints(getMaxHitPoints());
        setListOfPowers(type.getListOfPowers());
        setExperiencePoints(0);
        setLevel(1);//*
        setInventory( new Inventory() );
    }

    /**
     * this constructor tries to create a new Unit practically copied from the original,
     * but with no double memory references for objects
     * @param original
     * @returns copy Unit
     */

    public Unit (Unit original) {
        setName( original.getName() );
        setMaxHitPoints(original.getMaxHitPoints());
        setCurrentHitPoints(original.getCurrentHitPoints());
        setListOfPowers( new ListOfPowers() );
        //TODO add original.getListOfPowers() to current set of Powers
        setInventory( new Inventory() );
        original.getInventory().getItemList().forEach(this::add);
        setAbilityScores(original.getAbilityScores());
        setRace(original.getRace());
        setType(original.getType());
        setExperiencePoints(original.getExperiencePoints());
        setLevel(original.getLevel());
        isPlayerControlled(original.isPlayerControlled());
    }

    public boolean isPlayerControlled() {
        return isPlayerControlled;
    }

    public void isPlayerControlled(boolean playerControlled) {
        this.isPlayerControlled = playerControlled;
    }

    public int getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(int experiencePoints) {
        this.experiencePoints = experiencePoints;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    public int getCurrentInitiative() {
        return currentInitiative;
    }

    public void setCurrentInitiative(int currentInitiative) {
        this.currentInitiative = currentInitiative;
    }

    public UnitRace getRace() {
        return race;
    }

    public void setRace(UnitRace race) {
        this.race = race;
    }

    public AbilityScores getAbilityScores() {
        return abilityScores;
    }

    public void setAbilityScores(AbilityScores abilityScores) {
        this.abilityScores = abilityScores;
    }

    public UnitType getType() {
        return type;
    }

    public void setType(UnitType type) {
        this.type = type;
    }

    public int getCurrentHitPoints() {
        return currentHitPoints;
    }

    public void setCurrentHitPoints(int currentHitPoints) {
        this.currentHitPoints = Math.min( getMaxHitPoints(), currentHitPoints );
    }

    public int getMaxHitPoints() {
        return maxHitPoints;
    }

    /**
     * this method changes the maximum Hit Points
     * and such also adjusts the current hit points if changed
     * @param maxHitPoints
     */
    public void setMaxHitPoints(int maxHitPoints) {
        int previousHitPointMaximum = getMaxHitPoints();
        this.maxHitPoints = maxHitPoints;
        setCurrentHitPoints( getCurrentHitPoints() + maxHitPoints - previousHitPointMaximum );
    }

    public ListOfPowers getListOfPowers() {
        return listOfPowers;
    }

    protected void setListOfPowers(ListOfPowers listOfPowers) {
        this.listOfPowers = listOfPowers;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public void add(Item item) {
        getInventory().getItemList().add(item);
    }

    public void addItem(String itemName) {
        add( UnitCreator.getItemLibrary().getItem(itemName.toUpperCase()) );
    }

    public void addGold (int amount) {
        getInventory().addGold(amount);
    }

    public double getChallengeRating () {
        return getLevel() / 4.0;
    }

    public boolean isAlive () {
        return getCurrentHitPoints() > 0;
    }

    public int getAbilityScore (AbilityScore abilityScore) {
        return getAbilityScores().getAbilityScore(abilityScore);
    }

    public AbilityScore higherAbilityScoreOfTwo (AbilityScore abilityScore1, AbilityScore abilityScore2) {
        return getAbilityScore(abilityScore1) > getAbilityScore(abilityScore2) ? abilityScore1 : abilityScore2;
    }

    public int rollAbilityScore (AbilityScore abilityScore) {
        return Roll.d20() + getModifier(abilityScore) + getProficiency();
    }

    public boolean testSucces(AbilityScore abilityScore, int difficulty) {
        int roll = rollAbilityScore(abilityScore);
        System.out.printf("%s rolls %d to beat %d\n", getName(), roll, difficulty);
        return  roll >= difficulty;
    }

    public int getProficiency () {
        return (int) getLevel() * 2;
    }

    public int getModifier(AbilityScore abilityScore) {
        return getAbilityScores().getModifier(abilityScore);
    }

    /**
     * this method searches the Unit's inventory for any armor, if not, it acts as if it has clothes
     * if so, it searches for the one with the highest base Armor bonus, and returns that Armor object
     * @return
     */
    public Armor getBestArmor() {
        if (this.getInventory().getItemList().stream().anyMatch(item -> item instanceof Armor)) {
            int bestArmorBase = this.getInventory().getItemList().stream().filter(item -> item instanceof Armor).mapToInt(item -> ((Armor) item).getArmorBase()).max().getAsInt();
            return (Armor) this.getInventory().getItemList().stream().filter(item -> item instanceof Armor).filter(item -> ( (Armor) item ).getArmorBase() == bestArmorBase ).findFirst().get();
        } else {
            return ( (Armor) new ItemLibrary().getItem("Clothes") );
        }
    }

    /**
     * this method returns a Unit's Armor Class based on
     * it's Type, the best Armor in it's inventory
     * @return
     */
    public int getArmorClass() {

        return getType().getBaseArmorClass() + getBestArmor().getArmorBase() + getModifier( getBestArmor().getBaseAbilityScore() );

    }

    public int getSpeed() {
        return getRace().getBaseSpeed() + Math.max( getModifier(AbilityScore.DEX), getModifier(AbilityScore.CON) );
    }

    public void roll4Initiative() {
        setCurrentInitiative(Roll.d20() + getModifier(AbilityScore.DEX));
    }

    public int getLifeDiceSize() {
        return getType().getLifeDiceSize();
    }

    public int rollLifeDice() {
        return Roll.dX(getLifeDiceSize());
    }

    public void takesDamage (int value) {
        System.out.printf("%s takes %d damage\n", getName(), value);
        setCurrentHitPoints( Math.max( getCurrentHitPoints() - value , 0 ) );
    }

    public void heal (int value) {
        setCurrentHitPoints( getCurrentHitPoints() + value );
        System.out.printf("%s healed for %d HP to %d\n", getName(), value, getCurrentHitPoints());
    }

    public void takeShortRest () {
        for (int i = 0 ; i < getLevel() * 2 ; i++) {
            if (getCurrentHitPoints() < getMaxHitPoints()) {
                heal(Roll.dX(getLifeDiceSize()));
            }
        }
    }

    public void takeLongRest () {
        setCurrentHitPoints( getMaxHitPoints() );
    }

    public String getAbilityScoresStatsString() {
        StringBuilder temp = new StringBuilder("Stats: ");
        Stream.of(AbilityScore.values()).forEach(as -> temp.append(as.getName())
                                                            .append(": ")
                                                            .append( getAbilityScores().getAbilityScoresMap().get( as ) )
                                                            .append(" "));
        temp.append("\n");
        return temp.toString();
    }

    public String getCombatStatsString () {
        StringBuilder temp = new StringBuilder("Combat Stats: ");
        temp.append("MaxHP: ").append(getMaxHitPoints())
                .append(" CurrentHP: ").append(getCurrentHitPoints())
                .append(" Armor Class: ").append(getArmorClass());
        return temp.toString();
    }

    @Override
    public String toString() {
        return "Name: " + getName() + "\n" +
                getAbilityScoresStatsString() +
                getCombatStatsString();
    }
}
