package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.util.KeyboardUtil;

import java.io.Serializable;

/**
 * this class lets a user create a new player character
 * and created Monster Units with small corrections
 */
public class UnitCreator implements Serializable {
    /**
     * this variable loads in all predefined items in the ItemLibrary constructor
     */
    public static final ItemLibrary itemLibrary = new ItemLibrary();

    /**
     * this static method, asks the user to chose a name, race and type,
     * the method creates a new Unit based on the specifications
     * and adds corresponding items to the Unit's inventory
     * @return
     */
    public static Unit createPlayerCharacter() {
        System.out.println("--== Creating Player ==--");
        //TODO dit mag met 4d6 drop de lowest randomised kunnen gemaakt worden
        System.out.println("1. standaard array [16, 14, 14, 13, 11, 8]\n2. roll for stats\n<< Momenteel enkel Standaard Array >>");
        System.out.println("What's your characters name?");
        Unit temp =  new Unit (
                            KeyboardUtil.askForString(),
                            KeyboardUtil.askForRace(),
                            KeyboardUtil.askForType(),
                            true
                            );
        switch ( temp.getType() ) {
            case FIGHTER:
                temp.addItem( "Sword" );
                temp.addItem( "Chainmail" );
                temp.addGold(10);
                break;
            case RANGER:
                temp.addItem( "Sword" );
                temp.addItem( "Bow" );
                temp.addItem( "Leather armor" );
                temp.addGold(10);
                break;
            case HEALER:
                temp.addItem( "Holy symbol" );
                temp.addItem( "Dagger" );
                temp.addItem( "Clothes" );
                temp.addGold(10);
                break;
            default:
                break;
        }
        System.out.println(temp);
        return temp;
    }

    /**
     * this static method creates a Unit, based on the given name, race and type,
     * but then hard corrects it's Maximum Hit Points, Level (CR) and stats to balance out Players
     * @param name
     * @param unitRace
     * @param unitType
     * @param hitPointMaximum
     * @param challengeRating
     * @return
     */
    public static Unit createMonster (String name, UnitRace unitRace, UnitType unitType, int hitPointMaximum, double challengeRating) {
        Unit temp = new Unit (name, unitRace, unitType, false);
        temp.setMaxHitPoints(hitPointMaximum);
        temp.setLevel(challengeRating / 4);
        //SET ALL MONSTERS BASIC STATS TO 10
        temp.setAbilityScores( new AbilityScores(10) );
        //THEN CORRECT THEM WITH THEIR RESPECTIVE RACE CHANGES
        temp.getAbilityScores().add( temp.getRace().getAbilityScoreBonuses() );
        return temp;
    }


    //TODO dit heeft nog geen utility
    public static void rollRandomStats (Unit unit) {
        /*
        TODO verkrijg 4d6 drop de lowest
        TODO verkrijg dit 6 keer in een array
        TODO sorteer deze
        TODO vervang per keer de 16 door de hoogste, de 14 door de tweede hoogste, de andere 14 door de derde hoogste, ext
        */
        if(unit.isPlayerControlled()) {
            unit.getAbilityScores().add( unit.getType().getAbilityScoreBonuses() );
        }
        unit.getAbilityScores().add( unit.getRace().getAbilityScoreBonuses() );
    }

    public static ItemLibrary getItemLibrary() {
        return itemLibrary;
    }
}
