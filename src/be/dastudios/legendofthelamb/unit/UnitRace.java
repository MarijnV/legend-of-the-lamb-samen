package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.unit.AbilityScores;

import java.io.Serializable;

/**
 * this enum contains races
 * it determines a Unit's base speed and has minor Ability Score boosts or Penalties
 */
public enum UnitRace implements Serializable {
    HUMAN(6, new AbilityScores(1, 1, 1, 1, 1, 1)),
    DWARF(5, new AbilityScores(2, 0, 2, 0, 0, 0)),
    ELF(6, new AbilityScores(0, 2, 0, 0, 2, 0)),
    GOBLIN(6, new AbilityScores(-4, 2, 1, 0, -2, 0)),
    BUGBEAR(6, new AbilityScores(2, 2, 1, -2, 1, -2)),
    HOBGOBLIN(6, new AbilityScores(2, 0, 1, 1, 0, 0)),
    WOLF(8, new AbilityScores(2, 4, 2, -6, 2, -4)),
    TROLL(6, new AbilityScores(8, 2, 10, -3, -1, -3));

    int baseSpeed;
    AbilityScores abilityScoreBonuses;

    UnitRace(int baseSpeed, AbilityScores abilityScoreBonuses) {
        setBaseSpeed(baseSpeed);
        setAbilityScoreBonuses(abilityScoreBonuses);
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }

    public void setBaseSpeed(int baseSpeed) {
        this.baseSpeed = baseSpeed;
    }

    public AbilityScores getAbilityScoreBonuses() {
        return abilityScoreBonuses;
    }

    public void setAbilityScoreBonuses(AbilityScores abilityScoreBonuses) {
        this.abilityScoreBonuses = abilityScoreBonuses;
    }
}
