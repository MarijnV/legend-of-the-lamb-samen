package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.unit.AbilityScores;
import be.dastudios.legendofthelamb.unit.ListOfPowers;

import java.io.Serializable;

/**
 * this enum contains available UnitTypes
 * this determines a Base Armor Class, LifeDiceSize, priority of Ability Scores, some AbilityScore bonuses
 * and later powers
 */

public enum UnitType implements Serializable {
    FIGHTER(8, 12, new AbilityScores(16,14,13,8,14,11), new AbilityScores(2,0,1,0,0,0), new ListOfPowers() ),
    RANGER(6, 8, new AbilityScores(14,11,14,13,8,16), new AbilityScores(0,2,1,0,0,0), new ListOfPowers() ),
    HEALER(4, 6, new AbilityScores(13,8,14,11,14,16), new AbilityScores(0,0,0,2,0,1), new ListOfPowers() ),
    BEAST(7, 10, new AbilityScores(14,14,13,6,10,6), new AbilityScores(), new ListOfPowers() );


    int baseArmorClass;
    int lifeDiceSize;
    AbilityScores initialAbilityScoreArray;
    AbilityScores abilityScoreBonuses;
    ListOfPowers listOfPowers;

    UnitType(int baseArmorClass, int lifeDiceSize, AbilityScores initialAbilityScoreArray, AbilityScores abilityScoreBonuses, ListOfPowers listOfPowers) {
        setBaseArmorClass(baseArmorClass);
        setLifeDiceSize(lifeDiceSize);
        setInitialAbilityScoreArray(initialAbilityScoreArray);
        setAbilityScoreBonuses(abilityScoreBonuses);
        setListOfPowers(listOfPowers);
    }

    public AbilityScores getInitialAbilityScoreArray() {
        return this.initialAbilityScoreArray;
    }

    public void setInitialAbilityScoreArray(AbilityScores initialAbilityScoreArray) {
        this.initialAbilityScoreArray = initialAbilityScoreArray;
    }

    public AbilityScores getAbilityScoreBonuses() {
        return this.abilityScoreBonuses;
    }

    public void setAbilityScoreBonuses(AbilityScores abilityScoresBonuses) {
        this.abilityScoreBonuses = abilityScoresBonuses;
    }

    public int getBaseArmorClass() {
        return baseArmorClass;
    }

    public void setBaseArmorClass(int baseArmorClass) {
        this.baseArmorClass = baseArmorClass;
    }

    public int getLifeDiceSize() {
        return lifeDiceSize;
    }

    public void setLifeDiceSize(int lifeDiceSize) {
        this.lifeDiceSize = lifeDiceSize;
    }

    public ListOfPowers getListOfPowers() {
        return listOfPowers;
    }

    public void setListOfPowers(ListOfPowers listOfPowers) {
        this.listOfPowers = listOfPowers;
    }


}
