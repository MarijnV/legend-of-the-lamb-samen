package be.dastudios.legendofthelamb.unit;

import be.dastudios.legendofthelamb.unit.AbilityScore;
import be.dastudios.legendofthelamb.unit.Item;

import java.io.Serializable;

public class Weapon extends Item implements Serializable {
    private AbilityScore baseAbilityScore;
    private int damageDiceSize;

    public Weapon () {

    }

    public Weapon(String itemName, AbilityScore baseAbilityScore, int damageDiceSize) {
        super(itemName);
        setBaseAbilityScore(baseAbilityScore);
        setDamageDiceSize(damageDiceSize);
    }

    public AbilityScore getBaseAbilityScore() {
        return baseAbilityScore;
    }

    public void setBaseAbilityScore(AbilityScore baseAbilityScore) {
        this.baseAbilityScore = baseAbilityScore;
    }

    public int getDamageDiceSize() {
        return damageDiceSize;
    }

    public void setDamageDiceSize(int damageDiceSize) {
        this.damageDiceSize = damageDiceSize;
    }
}
