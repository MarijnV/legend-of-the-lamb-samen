package be.dastudios.legendofthelamb.util;

import be.dastudios.legendofthelamb.unit.UnitRace;
import be.dastudios.legendofthelamb.unit.UnitType;
import be.dastudios.legendofthelamb.travel.Direction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class KeyboardUtil implements Serializable {
    public static final InputStreamReader inputStreamReader = new InputStreamReader(System.in);
    public static final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

    /**
     * this method reads a line inputted by a user
     * and parses the first word for an integer
     * does so until it can
     * if an error occurs, it returns -1
     * @return
     */
    public static int askForNumber () {
        int temp = -1;
        while (temp < 1) {
            try {
                System.out.print("Number ");
                temp = Integer.parseInt(askForString().split(" ")[0]);
            } catch (NumberFormatException nfe) {
//                nfe.printStackTrace();
                System.out.println("We don't see the number here, try again");
                temp = -1;
            }
        }
        return temp;
    }

    /**
     * reads one line of input from the user, no limit
     * @return
     */
    public static String askForString() {
        String string = null;
        try {
                System.out.print("Input = ");
                string = bufferedReader.readLine();
        } catch (IOException ioe) {
            System.out.println("Input error");
            ioe.printStackTrace();
        }
        return string;
    }

    /**
     * displays the user the first 5 races
     * then asks the user for input
     * until a legal race is given
     * BUGNOTE: the user can technically input a race, not displayed
     * @return validated UnitRace
     */
    public static UnitRace askForRace () {
        System.out.println("What race do you want to play with?");
        Stream.of(UnitRace.values()).limit(5).forEach(System.out::println);
        String temp;
        do {
            temp = getFirstWordOfString(askForString()).toUpperCase();
        } while ( !cleanUpArraysToString(Arrays.toString( UnitRace.values() )).contains( " " + temp  + " ") );
        System.out.println("You chose " + temp.toLowerCase());
        return UnitRace.valueOf(temp);
    }

    /**
     * displays the user the first 3 unit types
     * then asks the user for input
     * until a legal type is given
     * BUGNOTE: the user can technically input a type, not displayed
     * @return validated UnitType
     */
    public static UnitType askForType () {
        System.out.println("What class/type do you want to play with?");
        Stream.of(UnitType.values()).limit(3).forEach(System.out::println);
        String temp;
        do {
            temp = getFirstWordOfString(askForString()).toUpperCase();
        } while ( !cleanUpArraysToString(Arrays.toString( UnitType.values() )).contains( " " + temp  + " ") );
        displayChoice(temp);
        return UnitType.valueOf(temp);
    }

    //TODO method that accepts a String and a enumClass (or array of generic objects) and
    // returns true if the String matches any the enum values

    public static String askForActionWord (ArrayList<String> actionWords) {
        String temp;
        do {
            temp = getFirstWordOfString( askForString() ).toUpperCase();
        } while(!actionWords.contains(temp));
        return temp;
    }

    //TODO this does not yet accept the HashSet of available Directions
    // so the user cannot move to an invalid direction
    public static Direction askForDirection() {
        String temp;
        while (true) {
            System.out.println("Type -direction- (north , south , east OR west) OR save (saves the game) OR look (shows the map)");
            temp = askForString().toUpperCase();
            //if (!getFirstWordOfString(temp).equals("GO")) continue;
            //if ( temp.split(" ").length < 2 ) continue;
            if (cleanUpArraysToString( Arrays.toString( Direction.values() ) ).contains(" " + getFirstWordOfString(temp) + " ")) break;
        }
        displayChoice(temp);
        return Direction.valueOf( getFirstWordOfString(temp) );
    }

    private static String getFirstWordOfString (String string) {
        return string.split(" ")[0];
    }

    private static String getSecondWordOfString (String string) {
        return string.split(" ")[1];
    }

    private static String cleanUpArraysToString (String string) {
        return string.replace('[', ' ').replace(']', ' ').replace(',', ' ');
    }

    private static void displayChoice (String string ) {
        System.out.println("You chose " + string.toLowerCase());
    }
}
