package be.dastudios.legendofthelamb.util;

import java.io.Serializable;
import java.util.Random;

/**
 * this class has static methods to simulate rolling polyhedral dice.
 * their is a generic dX method which accepts an number X and simulates a X-sided dice
 */
public class Roll implements Serializable {
    private static Random random = new Random();

    public static int dX (int x) {
        return random.nextInt(x) + 1 ;
    }

    public static int d4 () {
        return dX (4);
    }

    public static int d6 () {
        return dX (6);
    }

    public static int d8 () {
        return dX (8);
    }

    public static int d10 () {
        return dX (10);
    }

    public static int d12 () {
        return dX (12);
    }

    public static int d20 () {
        return dX (20);
    }

    /**
     * this method rolls a d20, but if true, gets the better of two rolls
     * if false, returns the lowest of two rolls
     * @param advantage
     * @return result of roll
     */
    public static int d20(boolean advantage) {
        if (advantage) {
            return Math.max(d20(), d20());
        } else {
            return Math.min(d20(), d20());
        }
    }

}
